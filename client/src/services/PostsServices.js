import Api from '../services/Api'

export default {
  getHome () {
    return Api().get('')
  },
  login (login, password) {
    return Api().post('/login', {'login': login, 'password': password})
  },
  getListUser () {
    return Api().get('/list')
  }
}
