var express = require('express');
var service = require('../service');
var router = express.Router();

var currentJson;
router.get('/', function (req, res) {
    res.send(
        {success: false}
    )
});
router.post('/login', function (req, res) {
    service.login(req.body.login, req.body.password)
        .then(json => {
            currentJson = service.showList(json, res)
            console.log("currentJson"+currentJson)
        });
});
router.get('/list', function (req, res) {

    res.send(
        currentJson
    );
});

module.exports = router;
