var fetch = require('node-fetch'), data;

function checkStatus(res) {
    if (res.ok) {
        return res.text();
    }
    return res.json().then((json) => {
        throw Error(json.message);
    });
}

function parseJSON(text) {
    return text ? JSON.parse(text) : {};
}

function wrapFetch(url, options, contentType = 'application/json; charset=utf-8') {
    const headers = {};
    if (contentType !== null) {
        headers['Content-Type'] = contentType;
        headers.Accept = contentType;
    }
    return fetch(url, {
        headers,
        ...options,
    }).then(checkStatus)
        .then(parseJSON)
        .catch(msg => console.log(msg));
}

function getUsers() {
    return wrapFetch(`https://reqres.in/api/users?page=2`, {
        method: 'GET',
    });
}


function login(email, password) {
    return wrapFetch(`https://reqres.in/api/login`, {
            method: 'POST',
            body: JSON.stringify({email: email, password: password})
        }
    );
}

function showList(json, res) {
    if (json == null) {
        res.send({
            title: 'Ошибка входа',
            success: false
        });
    } else {
        service.getUsers()
            .then(json2 => {
                data1 = json2.data;
                res.send({
                    list: data1,
                    success: true
                });
            });
        return {
            list: data1,
            success: true
        }
    }
}

const service = {
    getUsers: getUsers,
    login: login,
    showList: showList
};

module.exports = service;
